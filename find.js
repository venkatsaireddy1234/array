function find(element,cb){
    if (!element || !cb){
        return null
    }else{
        for (let i=0; i<element.length; i++){
            if (cb(element[i]) === true){
                return element[i]
            }
        }
    }
}

module.exports = find;
