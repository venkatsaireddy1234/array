function map(array,cb){
    if(!array || !cb){
        return null
    }else{
        let newarray = [];
        for (let i=0; i<array.length; i++){
            newarray.push(cb(array[i],i,array));
        }return newarray;
    }
}  

module.exports = map;
