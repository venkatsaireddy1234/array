function flatten(elements){
    if (!elements || !Array.isArray(elements)){
        return null
    }else{
        let flat = [];
        for(let i =0; i<elements.length; i++){
            if (Array.isArray(elements[i])){
                flat = flat.concat(flatten(elements[i]));
            }else{
                flat.push(elements[i]);
            }
        }return flat;
    }
}
    

module.exports=flatten;
