function reduce(array,cb,startingvalue){
    if (!array || !cb){
        return null
    }else if(startingvalue === undefined){
        startingvalue = array[0]
        let current = startingvalue;
        for(let i=1; i<array.length;i++){
            current=cb(current,array[i],i,array);
        }return current
    }else {
        let current = startingvalue;
        for (let i=0;i<array.length;i++){
            current = cb(current,array[i],i,array);
        }return current
    }
}

module .exports = reduce;

