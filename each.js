function each(array,cb){
    if (!array || !Array.isArray(array) || !cb){
        return null
    }else{
        for (let i=0; i<array.length; i++){
            cb(array[i],i);
        }  
    }  
}


module.exports = each;


